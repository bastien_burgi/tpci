#On crée un dockerfile pour permettre de lancer l'application
#sans avoir besoin d'installer node sur notre machine
#il se place à la racine du projet téléchargé précédemment
FROM node
# On utilise une image de base "node"
COPY . .
#Je copie tout mes fichiers à la racine de mon docker
RUN npm install
#Permet d'intaller les dépendances
CMD ["npm", "test"]
#la commande pour lancer les tests
